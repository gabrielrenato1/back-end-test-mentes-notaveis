<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('App\Http\Controllers')->group(function ()
{
  Route::namespace('User')->prefix('users')->name('users.')->group(function ()
  {
      Route::post('/','UserController@store')->name('store');
      Route::get( '/','UserController@index')->name('index');
      Route::prefix('{user}')->group(function (){
          Route::get(     '/',            'UserController@show')->name('show');
          Route::put(     '/',            'UserController@update')->name('update');
          Route::delete(  '/',            'UserController@delete')->name('delete');
      });
  });

  Route::namespace('Address')->prefix('addresses')->name('addresses.')->group(function ()
  {
      Route::get( '/','AddressController@index')->name('index');
      Route::prefix('{address}')->group(function (){
          Route::get('/','AddressController@show')->name('show');
      });

  });

  Route::namespace('City')->prefix('cities')->name('cities.')->group(function ()
  {
      Route::get( '/','CityController@index')->name('index');
      Route::prefix('{city}')->group(function (){
          Route::get('/','CityController@show')->name('show');
          Route::get('count',            'CityController@count')->name('count');
      });
  });

  Route::namespace('State')->prefix('states')->name('states.')->group(function ()
  {
      Route::get( '/','StateController@index')->name('index');
      Route::prefix('{state}')->group(function (){
          Route::get('/','StateController@show')->name('show');
          Route::get('count',            'StateController@count')->name('count');
      });
  });
});
