# Back-End Test Mentes Notáveis

## Introdução

Esta aplicação em Laravel é parte do teste de admição do Mentes Notáveis. Consiste em uma API Restful que manipula um usuário e retorna os endereços, cidades e estados cadastrados no banco de dados.

Agradeço a Mentes Notáveis pela oportunidade.

## Instalando o Projeto após clonar

```
cd path_to_cloned_project
composer install
php artisan migrate
```

## Criando Mock Up Data

Eu criei um pequeno comando para criar 3 estados e 5 cidades.
```
php artisan db:populate
```

Os estados são, respectivamente com os ids 1,2,3: 

- São Paulo
- Rio de Janeiro
- Distrito Federal

E as cidades, com mesmo esquema de identificação:

- São Carlos
- São Paulo
- Rio de Janeiro
- Petrópolis
- Brasília

## Rotas

Após usar o comando php artisan serve, poderá acessar as seguintes rotas.
- GET      | mentes-notaveis/addresses
- GET      | mentes-notaveis/addresses/\{address}
- GET      | mentes-notaveis/cities
- GET      | mentes-notaveis/cities/\{city}
- GET      | mentes-notaveis/cities/\{city}/count
- GET      | mentes-notaveis/states
- GET      | mentes-notaveis/states/\{state}
- GET      | mentes-notaveis/states/\{state}/count
- POST     | mentes-notaveis/users
- GET      | mentes-notaveis/users
- GET      | mentes-notaveis/users/\{user}         
- PUT      | mentes-notaveis/users/\{user}
- DELETE   | mentes-notaveis/users/\{user}

## Conclusão

Além do mais esse foi o resultado do teste que foi pedido
