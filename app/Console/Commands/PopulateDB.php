<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\State;
use App\Models\City;

class PopulateDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:populate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create mockups datas for tables States and Cities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Importing Data..');
        $states = [
          ["name" => "São Paulo", "uf" => "SP", "cities" => [
                      ["name" => "São Carlos", "ddd" => "16"],
                      ["name" => "São Paulo", "ddd" => "11"]
                    ],
          ],
          ["name" => "Rio de Janeiro", "uf" => "RJ", "cities" => [
                      ["name" => "Rio de Janeiro", "ddd" => "21"],
                      ["name" => "Petrópolis", "ddd" => "24"],
                    ],
          ],
          ["name" => "Distrito Federal", "uf" => "DF", "cities" => [
                      ["name" => "Brasília", "ddd" => "61"]
                    ],
          ],
        ];

        foreach ($states as $key => $state) {
          $newState = new State($state);
          $newState->save();
          foreach ($state['cities'] as $key => $city) {
            $newCity = new City($city);
            $newCity->state_id = $newState->id;
            $newCity->save();
          }
        }

        $this->info('Done!');

    }
}
