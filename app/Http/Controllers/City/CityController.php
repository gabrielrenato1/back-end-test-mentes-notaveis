<?php

namespace App\Http\Controllers\City;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\City;

class CityController extends Controller
{
    public function index(){
      $cities = City::withCount('users')->get();

      foreach ($cities as $key => $city) {
        $city->load(['state']);
      }

      return response($cities);
    }

    public function show($id){
      $city = City::withCount('users')->where('id', $id)->get()->load(['state']);
      return response($city);
    }

    public function count(City $city){
      return response(["city" => $city->name, "count" => $city->users()->count()], 200);
    }
}
