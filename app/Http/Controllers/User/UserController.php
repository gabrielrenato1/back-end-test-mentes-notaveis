<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Address;

class UserController extends Controller
{
    public function store(Request $request){
      $this->validate($request,[
          'name'  => 'string|required',
          'email' => 'string|required|unique:users|email',
          'cpf'   => 'required|unique:users',
          'age'   => 'required|integer',
          'phone' => 'required',
          'address.street' => 'required',
          'address.number' => 'required',
          'address.city_id' => 'required|exists:cities,id',
          'address.state_id' => 'required|exists:states,id',
          'address.zip_code' => 'required',
          'address.complement' => 'sometimes',
      ]);

      try{
        $newUser = new User($request->except('address'));
        $newAddress = new Address($request->address);
        $newAddress->save();
        $newUser->address_id = $newAddress->id;
        $newUser->city_id = $newAddress->city_id;
        $newUser->state_id = $newAddress->state_id;

        $newUser->save();


      }catch (QueryException $exception) {
          return response($exception->errorInfo);
      }

      return response($newUser);
    }

    public function index(){
      $users = User::all();

      foreach ($users as $key => $user) {
        $user->load(['address','city','state']);
      }

      return response($users);
    }

    public function show(User $user){
      return response($user->load(['address','city','state']));
    }

    public function update(Request $request, User $user){
      $this->validate($request,[
          'name'  => 'string|sometimes',
          'email' => 'string|sometimes|unique:users|email',
          'cpf'   => 'sometimes|unique:users',
          'age'   => 'sometimes|integer',
          'phone' => 'sometimes',
          'address.street' => 'sometimes',
          'address.number' => 'sometimes',
          'address.city_id' => 'sometimes|exists:cities,id',
          'address.state_id' => 'sometimes|exists:state,id',
          'address.zip_code' => 'sometimes',
          'address.complement' => 'sometimes',
      ]);

      try{
        $user->fill($request->except('address'));

        $address = $user->address()->first()->fill($request->address);
        $address->fill($request->address);

        $address->save();

        $user->address_id = $address->id;
        $user->city_id = $address->city_id;
        $user->state_id = $address->state_id;

        $user->save();

      }catch (QueryException $exception) {
          return response($exception->errorInfo);
      }

      return response($user);
    }

    public function delete(User $user){
      $user->delete();
      return response(["message" => "User Deleted!"], 200);

    }
}
