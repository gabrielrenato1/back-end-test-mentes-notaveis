<?php

namespace App\Http\Controllers\Address;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Address;

class AddressController extends Controller
{
    public function index(){
      $addresses = Address::all();

      foreach ($addresses as $key => $address) {
        $address->load(['city','state']);
      }

      return response($address);
    }

    public function show(Address $address){
      return response($address->load(['city','state']));
    }
}
