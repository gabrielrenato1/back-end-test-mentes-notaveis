<?php

namespace App\Http\Controllers\State;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\State;

class StateController extends Controller
{
    public function index(){
      $states = State::withCount('users')->get();
      return response($states);
    }

    public function show($id){
      $state = State::withCount('users')->where('id', $id)->get();
      return response($state);
    }
    // TODO: Erro 404
    public function count(State $state){
      return response(["state" => $state->name, "count" => $state->users()->count()], 200);
    }
}
