<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'ddd',
        'state_id',
    ];

    protected $hidden = [
        'state_id',
    ];

    protected $append = [
        'population'
    ];

    //RELATIONSHIPS
    public function users(){
      return $this->hasMany(User::class);
    }

    public function state(){
      return $this->belongsTo(State::class);
    }

    public function addresses(){
      return $this->belongsToMany(Address::class);
    }
}
