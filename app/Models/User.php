<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'cpf',
        'age',
        'phone',
        'address_id',
        'state_id',
        'city_id',
    ];

    protected $hidden = [
        'address_id',
        'state_id',
        'city_id',
        'deleted_at',
    ];

    //RELATIONSHIPS
    public function address(){
      return $this->belongsTo(Address::class);
    }

    public function city(){
      return $this->belongsTo(City::class);
    }

    public function state(){
      return $this->belongsTo(State::class);
    }
}
