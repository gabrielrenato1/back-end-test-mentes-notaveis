<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'street',
        'number',
        'city_id',
        'state_id',
        'zip_code',
        'complement'
    ];

    protected $hidden = [
        'city_id',
        'state_id',
    ];

    //RELATIONSHIPS
    public function users(){
      return $this->belongsToMany(Address::class);
    }

    public function state(){
      return $this->belongsTo(State::class);
    }

    public function city(){
      return $this->belongsTo(City::class);
    }
}
