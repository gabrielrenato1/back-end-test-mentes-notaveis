<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'uf',
        // 'cities',
    ];

    protected $append = [
        'population'
    ];

    //RELATIONSHIPS
    public function users(){
      return $this->hasMany(User::class);
    }

    public function cities(){
      return $this->hasMany(City::class);
    }

    public function addresses(){
      return $this->belongsToMany(Address::class);
    }
}
