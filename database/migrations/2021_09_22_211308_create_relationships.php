<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table){
          $table->unsignedBigInteger('state_id')->before('created_at');
          $table->unsignedBigInteger('city_id')->before('created_at');
          $table->unsignedBigInteger('address_id')->before('created_at');

          $table->foreign('address_id')->references('id')->on('addresses');
          $table->foreign('city_id')->references('id')->on('cities');
          $table->foreign('state_id')->references('id')->on('states');
        });

        Schema::table('addresses', function(Blueprint $table){
          $table->unsignedBigInteger('city_id')->before('created_at');
          $table->unsignedBigInteger('state_id')->before('created_at');

          $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
          $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade');
        });

        Schema::table('cities', function(Blueprint $table){
          $table->unsignedBigInteger('state_id')->before('created_at');

          $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
